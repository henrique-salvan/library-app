export const environment = {

    production: true,

    storage: {
        key: "library-app",
    },

    getPrefixApi: () => "http://67.205.188.27:8080/api",

};
