import {NgModule}             from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [

    {
        path: "auth",
        loadChildren: () => import("./auth/auth.module").then((module) => module.AuthModule),
    },

    {
        path: "library",
        loadChildren: () => import("./library/library.module").then((module) => module.LibraryModule),
    },

    // importante manter sempre no final
    {
        path: "",
        redirectTo: "library",
        pathMatch: "full",
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
