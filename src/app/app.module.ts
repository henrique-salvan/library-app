import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {NgModule}                            from "@angular/core";
import {BrowserModule}                       from "@angular/platform-browser";
import {BrowserAnimationsModule}             from "@angular/platform-browser/animations";
import {TokenInterceptor}                    from "./@commons/http-interceptors/token.interceptor";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent}     from "./app.component";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
