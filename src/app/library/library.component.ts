import {Component, OnInit} from "@angular/core";
import {Router}            from "@angular/router";
import {AuthStorage}       from "../@commons/helpers/storage";
import {UserInterface}     from "../@commons/interfaces/models/user.interface";
import {LoginService}      from "../@commons/services/login.service";

@Component({
    selector: "app-library",
    templateUrl: "./library.component.html",
    styleUrls: ["./library.component.scss"]
})
export class LibraryComponent implements OnInit {

    public userLogged: UserInterface = AuthStorage.getLoggedUser();

    constructor(private loginService: LoginService, private router: Router) {
    }

    ngOnInit(): void {
    }

    logout() {
        this.loginService.logout().subscribe(httpResponse => {
            this.router.navigate(["/auth"]);
        });
    }

}
