import {Component, OnInit} from "@angular/core";
import {BookInterface}     from "../../../@commons/interfaces/models/book.interface";
import {BookService}       from "../../../@commons/services/book.service";

@Component({
    selector: "app-books-index",
    templateUrl: "./books-index.component.html",
    styleUrls: ["./books-index.component.scss"]
})
export class BooksIndexComponent implements OnInit {

    public books: BookInterface[] = [];

    constructor(private bookService: BookService) {
    }

    ngOnInit(): void {
        this.bookService.index().subscribe(httpResponse => {
            if (httpResponse.success && httpResponse.content) {
                this.books = httpResponse.content;
            } else {
                throw "Ops, aconteceu um problema";
            }
        });
    }

    public destroy(book: BookInterface) {
        if (book.id) {
            this.bookService.destroy(book.id).subscribe(() => this.ngOnInit());
        }
    }

}
