import {NgModule}              from "@angular/core";
import {CommonsModule}         from "../../@commons/commons.module";
import {BookService}           from "../../@commons/services/book.service";
import {CommentService}        from "../../@commons/services/comment.service";
import {IfAdmin}               from "../directives/if.admin";
import {BooksDetailsComponent} from "./books-details/books-details.component";
import {BooksIndexComponent}   from "./books-index/books-index.component";
import {BooksRoutingModule}    from "./books-routing.module";
import { BooksManipulateComponent } from './books-manipulate/books-manipulate.component';


@NgModule({
    declarations: [
        BooksIndexComponent,
        BooksDetailsComponent,
        IfAdmin,
        BooksManipulateComponent,
    ],
    imports: [
        CommonsModule,
        BooksRoutingModule,
    ],
    providers: [
        BookService,
        CommentService,
    ]
})
export class BooksModule {
}
