import {NgModule}                 from "@angular/core";
import {RouterModule, Routes}     from "@angular/router";
import {BooksDetailsComponent}    from "./books-details/books-details.component";
import {BooksIndexComponent}      from "./books-index/books-index.component";
import {BooksManipulateComponent} from "./books-manipulate/books-manipulate.component";

const routes: Routes = [
    {
        path: "",
        component: BooksIndexComponent
    },
    {
        path: "manipulate",
        component: BooksManipulateComponent,
    },
    {
        path: "manipulate/:bookId",
        component: BooksManipulateComponent,
    },
    {
        path: ":bookId",
        component: BooksDetailsComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BooksRoutingModule {
}
