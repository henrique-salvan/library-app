import {Component, OnInit}                  from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router}             from "@angular/router";
import {ClrLoadingState}                    from "@clr/angular";
import {BookInterface}                      from "../../../@commons/interfaces/models/book.interface";
import {BookService}                        from "../../../@commons/services/book.service";

@Component({
    selector: "app-books-manipulate",
    templateUrl: "./books-manipulate.component.html",
    styleUrls: ["./books-manipulate.component.scss"]
})
export class BooksManipulateComponent implements OnInit {

    public payload: FormGroup | undefined;

    public isSubmitting: boolean = false;

    public submitButtonState = ClrLoadingState.DEFAULT;

    constructor(
        protected bookService: BookService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router
    ) {
    }

    ngOnInit(): void {
        if (this.activatedRoute.snapshot.params["bookId"]) {
            this.bookService.show(this.activatedRoute.snapshot.params["bookId"]).subscribe(response => {
                this.defineFormPayload(response.content);
            });
        } else {
            this.defineFormPayload();
        }
    }

    protected defineFormPayload(book?: BookInterface | undefined) {

        if (!book) {

            book = {
                id: 0,
                title: "",
                resume: "",
                active: true,
            };

        }

        this.payload = new FormGroup({
            id: new FormControl(book.id),
            title: new FormControl(book.title, [Validators.required, Validators.minLength(2)]),
            resume: new FormControl(book.resume, [Validators.required, Validators.minLength(2)]),
            active: new FormControl(book.active),
        });

    }

    get id() {
        if (this.payload && this.payload.get("id")?.value) {
            return this.payload.get("id")?.value;
        }

        return 0;
    }

    public submit() {

        if (! this.payload) {
            return;
        }

        if (this.isSubmitting) {
            return;
        }

        if (!this.payload.valid) {
            this.payload.markAllAsTouched();
            return;
        }

        this.isSubmitting = true;
        this.submitButtonState = ClrLoadingState.LOADING;

        (
            this.id === 0
                ? this.bookService.create(this.payload.getRawValue())
                : this.bookService.update(this.id, this.payload.getRawValue())
        ).subscribe(response => {

            if (response.success) {
                this.router.navigate(["/library"]);
            }

            this.isSubmitting = false;
            this.submitButtonState = ClrLoadingState.DEFAULT;

        });

    }

}
