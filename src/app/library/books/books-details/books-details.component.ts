import {Component, OnInit}                  from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute}                     from "@angular/router";
import {BookInterface}                      from "../../../@commons/interfaces/models/book.interface";
import {CommentInterface}                   from "../../../@commons/interfaces/models/comment.interface";
import {BookService}                        from "../../../@commons/services/book.service";
import {CommentService}                     from "../../../@commons/services/comment.service";

@Component({
    selector: "app-books-details",
    templateUrl: "./books-details.component.html",
    styleUrls: ["./books-details.component.scss"]
})
export class BooksDetailsComponent implements OnInit {

    public book: BookInterface | undefined;
    public comments: CommentInterface[] = [];

    public commentPayload: FormGroup = new FormGroup({
        comment: new FormControl("", [Validators.required, Validators.minLength(2)]),
        book_id: new FormControl("", [Validators.required, Validators.min(0)]),
    });

    constructor(
        private bookService: BookService,
        private commentService: CommentService,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        let bookId = this.activatedRoute.snapshot.paramMap.get("bookId");

        if (bookId) {
            this.bookService.show(parseInt(bookId)).subscribe(httpResponse => {
                if (httpResponse.success && httpResponse.content) {
                    this.book = httpResponse.content;
                    this.commentPayload.get("book_id")?.setValue(this.book.id);
                    this.updateComments();
                } else {
                    throw "Ops, aconteceu um problema";
                }
            });
        }
    }

    public updateComments() {
        if (this.book?.id) {
            this.commentService.byBookId(this.book?.id).subscribe(httpResponse => {
                if (httpResponse.success && httpResponse.content) {
                    this.comments = httpResponse.content;
                }
            });
        }

    }

    public submitComment() {

        if (!this.commentPayload.valid) {
            this.commentPayload.markAllAsTouched();
            return;
        }

        this.commentService.create(this.commentPayload.getRawValue()).subscribe(httpResponse => {
            this.commentPayload.get("comment")?.setValue("");
            this.commentPayload.markAsUntouched();
            this.updateComments();
        });
    }

}
