import {NgModule}             from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LibraryRoutingGuard}  from "./library-routing.guard";
import {LibraryComponent}     from "./library.component";

const routes: Routes = [
    {
        path: "",
        component: LibraryComponent,
        canActivate: [LibraryRoutingGuard],
        children: [
            {
                path: "books",
                loadChildren: () => import("./books/books.module").then((module) => module.BooksModule),
            },

            // importante manter sempre no final
            {
                path: "",
                redirectTo: "books",
                pathMatch: "full",
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LibraryRoutingModule {
}
