import {NgModule}            from "@angular/core";
import {CommonsModule}       from "../@commons/commons.module";
import {LoginService}        from "../@commons/services/login.service";
import {LibraryRoutingGuard} from "./library-routing.guard";

import {LibraryRoutingModule} from "./library-routing.module";
import {LibraryComponent}     from "./library.component";


@NgModule({
    declarations: [
        LibraryComponent,
    ],
    imports: [
        CommonsModule,
        LibraryRoutingModule,
    ],
    providers: [
        LibraryRoutingGuard,
        LoginService,
    ]
})
export class LibraryModule {
}
