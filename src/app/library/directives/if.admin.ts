import {Directive, Input, TemplateRef, ViewContainerRef} from "@angular/core";
import {AuthStorage}                                     from "../../@commons/helpers/storage";
import {UserInterface, UserRole}                         from "../../@commons/interfaces/models/user.interface";

@Directive({
    selector: "[if-admin]",
})
export class IfAdmin {

    private loggedUser: UserInterface = AuthStorage.getLoggedUser();

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef
    ) {
    }

    @Input("if-admin") set render(onlyAdmin: boolean) {
        if (onlyAdmin && this.loggedUser.role === UserRole.ADMIN) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }

}
