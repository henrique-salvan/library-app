import {ModelInterface} from "./model.interface";
import {UserInterface}  from "./user.interface";

export interface CommentInterface extends ModelInterface {
    comment: string;
    book_id: number;

    user_id?: number;
    user?: UserInterface;
}
