import {ModelInterface} from "./model.interface";

export interface UserInterface extends ModelInterface {
    name: string;
    login: string;
    password: string;
    role: UserRole;
}

export enum UserRole {
    ADMIN = "admin",
    NORMAL = "client",
}
