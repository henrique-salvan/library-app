import {CommentInterface} from "./comment.interface";
import {ModelInterface}   from "./model.interface";

export interface BookInterface extends ModelInterface {
    title: string;
    resume: string;
    published_at?: string;
    active: boolean;

    comments?: CommentInterface[];
}
