import {CommonModule}                     from "@angular/common";
import {NgModule}                         from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClarityModule}                    from "@clr/angular";

@NgModule({
    imports: [
        CommonModule,
        ClarityModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        CommonModule,
        ClarityModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class CommonsModule {
}
