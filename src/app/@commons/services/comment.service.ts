import {Injectable}            from "@angular/core";
import {Observable}            from "rxjs";
import {HttpResponseInterface} from "../interfaces/http.response.interface";
import {CommentInterface}      from "../interfaces/models/comment.interface";
import {HttpService}           from "./http.service";

@Injectable()
export class CommentService extends HttpService<CommentInterface> {

    resource() {
        return "comments";
    }

    byBookId(bookId: number): Observable<HttpResponseInterface<CommentInterface[]>> {
        return this.httpClient.get<HttpResponseInterface<CommentInterface[]>>(`${this.uri}/${bookId}`);
    }

}
