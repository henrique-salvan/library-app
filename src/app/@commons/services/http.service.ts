import {HttpClient}            from "@angular/common/http";
import {Injectable}            from "@angular/core";
import {Observable}            from "rxjs";
import {environment}           from "../../../environments/environment";
import {HttpResponseInterface} from "../interfaces/http.response.interface";

@Injectable()
export abstract class HttpService<T> {

    protected readonly uri: string;

    public constructor(protected httpClient: HttpClient) {
        this.uri = `${environment.getPrefixApi()}/${this.resource()}`;
    }

    abstract resource(): string;

    index(): Observable<HttpResponseInterface<T[]>> {
        return this.httpClient.get<HttpResponseInterface<T[]>>(`${this.uri}`);
    }

    show(id: number): Observable<HttpResponseInterface<T>> {
        return this.httpClient.get<HttpResponseInterface<T>>(`${this.uri}/${id}`);
    }

    create(payload: any): Observable<HttpResponseInterface<T>> {
        return this.httpClient.post<HttpResponseInterface<T>>(`${this.uri}`, payload);
    }

    update(id: number, payload: any): Observable<HttpResponseInterface<T>> {
        return this.httpClient.put<HttpResponseInterface<T>>(`${this.uri}/${id}`, payload);
    }

    destroy(id: number): Observable<HttpResponseInterface<T>> {
        return this.httpClient.delete<HttpResponseInterface<T>>(`${this.uri}/${id}`);
    }

}
