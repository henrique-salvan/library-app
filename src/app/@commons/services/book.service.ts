import {Injectable}    from "@angular/core";
import {BookInterface} from "../interfaces/models/book.interface";
import {HttpService}   from "./http.service";

@Injectable()
export class BookService extends HttpService<BookInterface> {

    resource() {
        return "books";
    }

}
