import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {FormControl, FormGroup, Validators}   from "@angular/forms";
import {Router}                               from "@angular/router";
import {AuthStorage}                          from "../../@commons/helpers/storage";
import {LoginService}                         from "../../@commons/services/login.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

    public payload: FormGroup = new FormGroup({
        login: new FormControl("", [Validators.required, Validators.minLength(2)]),
        password: new FormControl("", [Validators.required, Validators.minLength(6)]),
    });

    constructor(
        private loginService: LoginService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
    }

    public submit() {

        if (!this.payload.valid) {
            this.payload.markAllAsTouched();
            return;
        }

        this.loginService.login(this.payload.getRawValue()).subscribe((httpResponse) => {
            if (httpResponse.success && httpResponse.content) {
                AuthStorage.setToken(httpResponse.content);
                this.router.navigate(["/library"]);
            } else {
                console.log("problema com o login");
            }
        });

    }

}
