import {NgModule}         from "@angular/core";
import {CommonsModule}    from "../@commons/commons.module";
import {LoginService}     from "../@commons/services/login.service";
import {AuthRoutingGuard} from "./auth-routing.guard";

import {AuthRoutingModule} from "./auth-routing.module";
import {AuthComponent}     from "./auth.component";
import {LoginComponent}    from "./login/login.component";


@NgModule({
    declarations: [AuthComponent, LoginComponent],
    imports: [
        CommonsModule,
        AuthRoutingModule
    ],
    providers: [
        AuthRoutingGuard,
        LoginService,
    ]
})
export class AuthModule {
}
