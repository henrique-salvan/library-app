import {NgModule}             from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AuthRoutingGuard}     from "./auth-routing.guard";
import {AuthComponent}        from "./auth.component";
import {LoginComponent}       from "./login/login.component";


const routes: Routes = [

    {
        path: "",
        component: AuthComponent,
        canActivate: [AuthRoutingGuard],
        children: [
            {path: "login", component: LoginComponent},
            {path: "", redirectTo: "login", pathMatch: "full"}
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {
}
