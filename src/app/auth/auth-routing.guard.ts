import {Injectable}                                                       from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable}                                                       from "rxjs";
import {LoginService}                                                     from "../@commons/services/login.service";

@Injectable()
export class AuthRoutingGuard implements CanActivate {

    constructor(
        private router: Router,
        private loginService: LoginService,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        return new Observable((observer) => {
            this.loginService.acknowledge().subscribe(httpResponse => {
                if (httpResponse.success) {
                    this.router.navigate(["/"]);
                    observer.next(false);
                } else {
                    observer.next(true);
                }
            });
        });

    }

}
